document.getElementById('container').onchange = function(){
    let bill = document.getElementById('billInput').value;
    let tipPercent = document.getElementById('tipInput').value;
    let split = document.getElementById('splitInput').value;
    
    
    let tipValue = bill * (tipPercent / 100);
    
    
    let newBillEach = (bill + tipValue) / split;
    
    document.getElementById('tipOutput').innerHTML = tipPercent + '%';

    document.getElementById('splitOutput').innerHTML = split;


    document.getElementById('newBill').innerHTML = newBillEach;

    document.getElementById('tipAmount').innerHTML = tipValue;
}